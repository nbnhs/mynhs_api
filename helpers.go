package main

import "google.golang.org/api/drive/v3"

type User struct {
	ID, First, RollFileID, RollGID string
	Drive                          *drive.Service
}

// NewUser tries to create a new User instance with the provided information. If the provided information doesn't match an applicable user, an error is returned instead.
func NewUser(id, first, rollFileID, rollGID string, drive *drive.Service) (User, error) {
	// TODO
}
