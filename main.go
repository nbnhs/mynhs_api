package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func switchboard(r *http.Request) (contentType string, statusCode int, result []byte) {
	query := r.URL.Query()
	switch query.Get("get") {
	case "roll":
		svc, err := Authenticate()
		if err != nil {
			log.Println(err.Error())
			return "text/plain", 401, []byte("The server encountered an error with authentication. Please try again later.")
		}

		user, err := NewUser(query.Get("id"), query.Get("first"), os.Getenv("ROLL_FILE_ID"), os.Getenv("ROLL_GID"), svc)
		if err != nil {
			return "text/plain", 401, []byte(err.Error())
		}
		return "text/plain", 200, []byte(GetCSVRow(user))
	default:

	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	headers := map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Methods": "GET, OPTIONS",
		"Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, X-Teach-Name1, X-Teach-Name2, X-Student-Name1, X-Student-Name2, X-Teach-Email, X-NHS-ID, X-NHS-ImageKind",
	}

	for key, val := range headers {
		w.Header().Set(key, val)
	}

	switch r.Method {
	case "GET":
		contentType, code, result := switchboard(r)
		w.Header().Set("Content-Type", contentType)
		w.WriteHeader(code)
		w.Write(result)
	default:
		w.WriteHeader(http.StatusOK)
		w.Write([]byte{})
	}

}

func main() {
	http.HandleFunc("/", handler)
	if os.Getenv("PORT") == "443" {
		log.Fatal(http.ListenAndServeTLS(":443", "../fullchain.pem", "../privkey.pem", nil))
	} else {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), nil))
	}
}
